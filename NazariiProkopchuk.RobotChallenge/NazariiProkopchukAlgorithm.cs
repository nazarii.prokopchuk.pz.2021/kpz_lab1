﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;

namespace NazariiProkopchuk.RobotChallenge
{
     public class NazariiProkopchukAlgorithm : IRobotAlgorithm
     {
          public const int COLLECT_DISTANCE = 2;

          public int MAX_ROBOT_AMOUNT = 70;

          public int MIN_ENERGY_FOR_CREATION = 250;
          public int NEW_ROBOT_ENERGY = 150;

          Dictionary<int, int> myRobotIndexes = new Dictionary<int, int>();
          Dictionary<int, int> robotsToStationIndexes = new Dictionary<int, int>();

          public int RoundCount { get; set; }
          public String Author { get => "Nazarii Prokopchuk"; }

          public bool init = false;
          public NazariiProkopchukAlgorithm() 
          {
               RoundCount = 0;
               Robot.Common.Logger.OnLogRound += Logger_OnLogRound;
          }

          public void Logger_OnLogRound(object sender, LogRoundEventArgs e)
          {
               RoundCount++;
          }

          public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
          {
               if(!init)
               {
                    for(int i = 0; i < robots.Count; i++)
                    {
                         if (robots[i].OwnerName == Author)
                         {
                              myRobotIndexes.Add(i, myRobotIndexes.Count);
                         }
                    }

                    for(int i = 0; i < map.Stations.Count; i++)
                    {
                         robotsToStationIndexes.Add(i, -1);
                    }

                    init = true;
               }

               if(!myRobotIndexes.ContainsKey(robotToMoveIndex))
               {
                    myRobotIndexes.Add(robotToMoveIndex, myRobotIndexes.Count);
               }

               var currentRobot = robots[robotToMoveIndex];

               if (currentRobot.Energy >= MIN_ENERGY_FOR_CREATION && IsNearStation(currentRobot, map) &&
                    robots.Where(r => r.OwnerName == Author).Count() < MAX_ROBOT_AMOUNT)
               {
                    return new CreateNewRobotCommand() { NewRobotEnergy = NEW_ROBOT_ENERGY};
               }

               if (IsOnStation(currentRobot, map))
                    return new CollectEnergyCommand();
               if(IsNearStation(currentRobot, map) && currentRobot.Energy < 30 && GetNearStation(currentRobot, map).Energy >= 30)
                    return new CollectEnergyCommand();

               Position targetPosition = FindNearestFreeStation(robots, robotToMoveIndex, map);

               if (targetPosition.Equals(currentRobot.Position))
                    targetPosition = FindNearestEnemyStation(robots, robotToMoveIndex, map);
               if (targetPosition.Equals(currentRobot.Position) && IsNearStation(currentRobot, map))
                    return new CollectEnergyCommand();

               Position nextCell = NextCell(robots[robotToMoveIndex], targetPosition);
               return new MoveCommand()
               {
                    NewPosition = nextCell,
               };
          }


          public Position FindNearestFreeStation(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
          {

               List<EnergyStation> freeStations = new List<EnergyStation>();

               foreach(EnergyStation station in map.Stations)
               {
                    bool free = true;
                    foreach(Robot.Common.Robot robot in robots)
                    {
                         if ((station.Position.Equals(robot.Position)) ||
                              (robotsToStationIndexes[map.Stations.IndexOf(station)] != -1 &&
                              robotsToStationIndexes[map.Stations.IndexOf(station)] != robotToMoveIndex))
                         {
                              free = false;
                              break;
                         }
                    }
                    if(free)
                         freeStations.Add(station);
               }

               if (freeStations.Count > 0)
               {
                    EnergyStation targetStation = freeStations.OrderBy(s => DistanceHelper.FindDistance(robots[robotToMoveIndex].Position, s.Position)).ElementAt(0);
                    robotsToStationIndexes[robotsToStationIndexes.FirstOrDefault(x => x.Value == robotToMoveIndex).Key] = -1;
                    robotsToStationIndexes[map.Stations.IndexOf(targetStation)] = robotToMoveIndex;
                    return targetStation.Position;
               }
               return robots[robotToMoveIndex].Position;
          }

          public Position FindNearestEnemyStation(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
          {

               List<EnergyStation> enemyStations = new List<EnergyStation>();

               foreach (EnergyStation station in map.Stations)
               {
                    foreach (Robot.Common.Robot robot in robots)
                    {
                         if (robot.OwnerName != Author)
                         {
                              if (station.Position.Equals(robot.Position)&&
                                   !(robotsToStationIndexes[map.Stations.IndexOf(station)] != -1 &&
                              robotsToStationIndexes[map.Stations.IndexOf(station)] != robotToMoveIndex))
                              {
                                   enemyStations.Add(station);
                              }
                         }
                    }
               }

               if (enemyStations.Count > 0)
               {
                    EnergyStation targetStation = enemyStations.OrderBy(s => DistanceHelper.FindDistance(robots[robotToMoveIndex].Position, s.Position)).ElementAt(0);
                    robotsToStationIndexes[robotsToStationIndexes.FirstOrDefault(x => x.Value == robotToMoveIndex).Key] = -1;
                    robotsToStationIndexes[map.Stations.IndexOf(targetStation)] = robotToMoveIndex;
                    return targetStation.Position;
               }
               return robots[robotToMoveIndex].Position;
          }

          public bool IsOnStation(Robot.Common.Robot robot, Map map)
          {
               return map.Stations.Where(s => s.Position.X == robot.Position.X && s.Position.Y == robot.Position.Y).Count() != 0;
          }

          public bool IsNearStation(Robot.Common.Robot robot, Map map)
          {
               bool result = false;

               for (int i = -COLLECT_DISTANCE; i <= COLLECT_DISTANCE; i++)
               {
                    for (int j = -COLLECT_DISTANCE; j <= COLLECT_DISTANCE; j++)
                    {
                         if (map.Stations.Where(s => s.Position.X + i == robot.Position.X && s.Position.Y + j == robot.Position.Y).Count() != 0)
                         {
                              result = true;
                              break;
                         }
                    }
                    if (result)
                         break;
               }
               return result;
          }

          public EnergyStation GetNearStation(Robot.Common.Robot robot, Map map)
          {
               for (int i = -COLLECT_DISTANCE; i <= COLLECT_DISTANCE; i++)
               {
                    for (int j = -COLLECT_DISTANCE; j <= COLLECT_DISTANCE; j++)
                    {
                         if (map.Stations.Where(s => s.Position.X + i == robot.Position.X && s.Position.Y + j == robot.Position.Y).Count() != 0)
                         {
                              return map.Stations.Where(s => s.Position.X + i == robot.Position.X && s.Position.Y + j == robot.Position.Y).ElementAt(0);
                         }
                    }
               }
               return null;
          }

          public Position NextCell(Robot.Common.Robot robot, Position target)
          {
               if(robot.Position.Equals(target))
                    return robot.Position;
               // Обчислюємо кількість кроків, необхідних для досягнення цілі.
               int steps = CalculateStepsToDestination(robot, target);
               

               // Якщо досягнення цілі неможливе за залишок раундів або взагалі неможливе, повертаємо поточну позицію робота.
               if (steps > 49 - RoundCount || steps == -1)
               {
                    return robot.Position;
               }
               // Розраховуємо кроки по осях X і Y для руху до цілі.
               double deltaX = (target.X - robot.Position.X) / (double)steps;
               double deltaY = (target.Y - robot.Position.Y) / (double)steps;

               // Обчислюємо нову позицію робота з урахуванням кроків.
               int newX = (int)Math.Round(robot.Position.X + deltaX);
               int newY = (int)Math.Round(robot.Position.Y + deltaY);

               Position newPosition = new Position(newX, newY);
               return newPosition;
          }

          public int CalculateStepsToDestination(Robot.Common.Robot robot, Position target, bool pushing = false)
          {
               // Розрахунок відстані в клітинках між поточною позицією робота і ціллю.
               int dist = CalculateManhattanDistance(robot.Position, target);

               // Якщо робот вже на цілі, не потрібно робити жодного кроку.
               if (dist == 0) return 0;

               // Якщо відстань до цілі більша, ніж енергія робота, ціль недосяжна.
               if (dist > robot.Energy)
               {
                    return -1;
               }

               // Якщо енергія досить для пройдення одного кроку, то повертаємо 1 крок.
               if (dist * dist <= robot.Energy)
               {
                    return 1;
               }
               int steps = 2;
               while (steps <= 10)
               {
                    // Розраховуємо, скільки енергії потрібно на кожен крок.
                    List<int> cells = Divide(dist, steps).ToList();
                    int energyNeed = 0;
                    foreach (int cell in cells)
                    {
                         energyNeed += cell * cell;
                    }
                    // Якщо використовується спихання, перевіряємо, чи є достатньо енергії з урахуванням додаткових витрат на спихання.
                    if (pushing)
                    {
                         if (energyNeed <= robot.Energy - 10)
                         {
                              return steps;
                         }
                    }
                    else
                    {
                         // В іншому випадку перевіряємо, чи є достатньо енергії для проходження без спихання.
                         if (energyNeed <= robot.Energy)
                         {
                              return steps;
                         }
                    }
                    steps++;
               }
               // Якщо досягнення цілі не вдається за 10 кроків, ціль вважається недосяжною.
               return -1;
          }
          public static int[] Divide(int a, int b)
          {
               if (b == 0)
               {
                    throw new ArgumentException("Ділення на нуль неможливе.");
               }

               int quotient = a / b; // Отримуємо цілу частину результату ділення
               int remainder = a % b; // Отримуємо залишок

               int[] result = new int[b];

               // Заповнюємо масив значеннями цілої частини результату
               for (int i = 0; i < b; i++)
               {
                    result[i] = quotient;
               }

               // Розподіляємо залишок на масив значень
               for (int i = 0; i < remainder; i++)
               {
                    result[i]++;
               }

               return result;
          }
          public static int CalculateManhattanDistance(Position a, Position b)
          {
               int deltaX = Math.Abs(a.X - b.X);
               int deltaY = Math.Abs(a.Y - b.Y);

               // Манхеттенська відстань: сума горизонтальних і вертикальних переміщень
               int distance = deltaX + deltaY;

               return distance;
          }

          public static class DistanceHelper
          {
               public static int FindDistance(Position a, Position b)
               {
                    return (int)(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
               }
          }
     }   
}
